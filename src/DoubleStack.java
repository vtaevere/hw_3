/* VIITED
 * 
 *  [1] http://enos.itcollege.ee/~jpoial/algoritmid/adt.html
 *  [2] https://www.programcreek.com/2012/12/leetcode-evaluate-reverse-polish-notation/
 *  [3] http://www.java2s.com/Code/Java/Collections-Data-Structure/ReversePolishNotation.htm
 *  
 *  */

import java.util.*;

public class DoubleStack {

   public static void main (String[] argum) {
      System.out.println(DoubleStack.interpret("2. 15. -"));
   }
   
   private LinkedList<Double> list = new LinkedList<Double>();

   DoubleStack() {
	   
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
	  try { 
		  DoubleStack tmp = new DoubleStack();
		  tmp.list.addAll(list);
		  return tmp;
	  } catch (Exception e) {
		  throw new CloneNotSupportedException("Cloning failed");		  
	  }
   }

   public boolean stEmpty() {
      return list.isEmpty();
   }

   public void push (double a) {
	   list.push(a);
   }

   public double pop() {
      if (stEmpty()) {
    	  throw new RuntimeException("Stack is empty!");
      } else {
    	  return list.pop();
      }
   }

// [2]
   public void op (String s) {
	   if (!stEmpty()) {
		   double op2 = list.pop();
		   double op1 = list.pop();
		   switch (s) {
		   case "+" :
			   list.push(op2 + op1);
			   break;
		   case "-" :
			   list.push(op1 - op2);
			   break;
		   case "*" :
			   list.push(op2 * op1);
			   break;
		   case "/" :
			   list.push(op1 / op2);
			   break;			   
		   } 
		 } else {
			  throw new RuntimeException("Could not calculate - stack is empty");		   
	   }     
   }
  
   public double tos() {
      if (stEmpty()) {
    	  throw new RuntimeException("List is empty");
      } else {
    	  return list.peek();
      }
   }

   @Override
   public boolean equals (Object o) {
	   return this.list.equals(((DoubleStack) o).list);
   }

   @Override
   public String toString() {
      if (stEmpty()) {
    	  System.out.println("Error converting to string - the list is empty");
      } 
      
      StringBuilder stringbuffer = new StringBuilder();
      for (int i = list.size() - 1; i >= 0; i--) {
          stringbuffer.append(String.valueOf(list.get(i) + " "));
      }
      return stringbuffer.toString();
      
    	  
      }
   
   public static boolean nextIsOperator(String next) {
	    return (next.equals("+") || next.equals("-") || next.equals("*") || next
	        .equals("/"));
	  }
   
// [3]
   public static double interpret (String pol) {
	    String input = pol.trim();
	    // scanner to manipulate input and stack to store double values
	    String next;
	    Stack<Double> stack = new Stack<Double>();
	    Scanner scan = new Scanner(pol);

	    // Loop while there are tokens left in scan
	    while (scan.hasNext()) {
	      // Retrieve the next token from the input
	      next = scan.next();

	      // See if token is a mathematical operator
	      if (nextIsOperator(next)) {
	        // Ensure there are enough numbers on stack
	        if (stack.size() > 1) {
	          if (next.equals("+")) {
	            stack.push((Double) stack.pop() + (Double) stack.pop());
	          } else if (next.equals("-")) {
	            stack.push(-(Double) stack.pop() + (Double) stack.pop());
	          } else if (next.equals("*")) {
	            stack.push((Double) stack.pop() * (Double) stack.pop());
	          } else if (next.equals("/")) {
	            double first = stack.pop();
	            double second = stack.pop();

	            if (first == 0) {
	              throw new RuntimeException("The RPN equation attempted to divide by zero.");
	            } else {
	              stack.push(second / first);
	            }
	          }
	        } else {
	          throw new RuntimeException("A mathematical operator occured before there were enough numerical values for it to evaluate.");
	        }
	      } else {
	        try {
	          stack.push(Double.parseDouble(next));
	        } catch (NumberFormatException c) {
	          throw new RuntimeException("The string is not a valid RPN equation.");
	        }
	      }
	    }

	    if (stack.size() > 1) {
	      throw new RuntimeException("There are too many numbers and not enough mathematical operators with which to evaluate them.");
	    }

	    return (Double) stack.pop();
   }

}

